<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $employees = Employee::all();
        //dd($employees);
        return view('forms.employee', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('forms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $employee = new Employee();
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->middle_name = $request->middle_name;
        $employee->address = $request->address;
        $employee->birthdate = $request->birthdate;
        $employee->age = $request->age;
        $employee->contact_no = $request->contact_no;
        $employee->father_name = $request->father_name;
        $employee->mother_name = $request->mother_name;
        $employee->school = $request->school;
        $employee->save();
        return redirect('/employee');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
        return view('forms.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
        //dd($request->first_name);

        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->middle_name = $request->middle_name;
        $employee->address = $request->address;
        $employee->birthdate = $request->birthdate;
        $employee->age = $request->age;
        $employee->contact_no = $request->contact_no;
        $employee->father_name = $request->father_name;
        $employee->mother_name = $request->mother_name;
        $employee->school = $request->school;
        $employee->save();
        return view('forms.edit', compact('employee'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
        //dd($employee);
        $employee->delete();
        return redirect('/employee');
    }
    public function search(Request $request){
        $search = $request->search;
        if($search != null){
        $employees = Employee::where('first_name', 'like', '%' .$search. '%')->orWhere('last_name', 'like', '%' .$search. '%')->get();
        return view('forms.employee', compact('employees'));
        } else{
            return redirect('/employee');
        }
    }
    public function fetchData(){
        $data = Employee::all();
        return json_encode($data);
    }
}
