$(document).ready( function () {
    $('#myTable').DataTable({
    	responsive:true,
    	processing:true,
    	serverside:true,

    	ajax:{
    		type: 'GET',
    		url: '/data',
    		dataType: 'json',
    		dataSrc: ''
    	},
    	"columns": [
    	{data: 'id'},
    	{data: 'first_name'},
    	{data: 'last_name'},
    	{data: 'middle_name'},
    	{data: 'address'},
    	{data: 'birthdate'},
    	{data: 'age'},
    	{data: 'contact_no'},
    	{data: 'father_name'},
    	{data: 'mother_name'},
    	{data: 'school'},

    	]
    });
} );