@extends('layouts.app')

@section('content')
<div class="container">
	<h3 class="text-center">Edit Employee Details</h3>
	<div class="col-lg-6 offset-3">
		<form action="/employee/{{$employee->id}}" method="POST" enctype="multipart/form-data">
			@csrf
			@method("PUT")
			<div class="form-group">
				<label>First Name</label>
				<input type="text" name="first_name" class="form-control" value="{{$employee->first_name}}">
			</div>

			<div class="form-group">
				<label>Last Name</label>
				<input type="text" name="last_name" class="form-control" value="{{$employee->last_name}}">
			</div>

			<div class="form-group">
				<label>Middle Name</label>
				<input type="text" name="middle_name" class="form-control" value="{{$employee->middle_name}}">
			</div>

			<div class="form-group">
				<label>Address</label>
				<input type="text" name="address" class="form-control" value="{{$employee->address}}">
			</div>

			<div class="form-group">
				<label>Birthdate</label>
				<input type="text" name="birthdate" class="form-control" value="{{$employee->birthdate}}">
			</div>

			<div class="form-group">
				<label>Age</label>
				<input type="number" name="age" class="form-control" value="{{$employee->age}}">
			</div>


			<div class="form-group">
				<label>Contact No.</label>
				<input type="number" name="contact_no" class="form-control" value="{{$employee->contact_no}}">
			</div>

			<div class="form-group">
				<label>Father Name</label>
				<input type="text" name="father_name" class="form-control" value="{{$employee->father_name}}">
			</div>

			<div class="form-group">
				<label>Mother Name</label>
				<input type="text" name="mother_name" class="form-control" value="{{$employee->mother_name}}">
			</div>

			<div class="form-group">
				<label>School</label>
				<input type="text" name="school" class="form-control" value="{{$employee->school}}">
			</div>

			<button class="btn btn-primary mb-2">Edit Employee</button>
		</form>
	</div>
</div>
@endsection