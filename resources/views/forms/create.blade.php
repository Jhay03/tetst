@extends('layouts.app')

@section('content')
<div class="container">
	<h3 class="text-center">Employee Form</h3>
	<div class="col-lg-6 offset-3">
		<form action="/employee" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="form-group">
				<label>First Name</label>
				<input type="text" name="first_name" class="form-control">
			</div>

			<div class="form-group">
				<label>Last Name</label>
				<input type="text" name="last_name" class="form-control">
			</div>

			<div class="form-group">
				<label>Middle Name</label>
				<input type="text" name="middle_name" class="form-control">
			</div>

			<div class="form-group">
				<label>Address</label>
				<input type="text" name="address" class="form-control">
			</div>

			<div class="form-group">
				<label>Birthdate</label>
				<input type="text" name="birthdate" class="form-control">
			</div>

			<div class="form-group">
				<label>Age</label>
				<input type="number" name="age" class="form-control">
			</div>


			<div class="form-group">
				<label>Contact No.</label>
				<input type="number" name="contact_no" class="form-control">
			</div>

			<div class="form-group">
				<label>Father Name</label>
				<input type="text" name="father_name" class="form-control">
			</div>

			<div class="form-group">
				<label>Mother Name</label>
				<input type="text" name="mother_name" class="form-control">
			</div>

			<div class="form-group">
				<label>School</label>
				<input type="text" name="school" class="form-control">
			</div>

			<button class="btn btn-success mb-2">Add Employee</button>
		</form>
	</div>
</div>
@endsection