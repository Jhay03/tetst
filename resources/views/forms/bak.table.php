			@foreach($employees as $employee)
			<tbody>
				<tr>
					<td>{{$employee->id}}</td>
					<td>{{$employee->first_name}}</td>
					<td>{{$employee->last_name}}</td>
					<td>{{$employee->middle_name}}</td>
					<td>{{$employee->address}}</td>
					<td>{{$employee->birthdate}}</td>
					<td>{{$employee->age}}</td>
					<td>{{$employee->contact_no}}</td>
					<td>{{$employee->father_name}}</td>
					<td>{{$employee->mother_name}}</td>
					<td>{{$employee->school}}</td>
					<td>
						<a href="/employee/{{$employee->id}}/edit" class="btn btn-warning mb-2">Edit</a>
						<form action="/employee/{{$employee->id}}" method="POST" enctype="multipart/form-data">
							@csrf
							@method("DELETE")
						<button class="btn btn-danger">Delete</button>
							
						</form>
					</td>
				</tr>
			</tbody>
			@endforeach