@extends('layouts.app')

@section('content')
<div class="container">
	<h3 class="text-center mt-2">List of Employees</h3>

	<div class="row">
		<!--<form action="/employee/search" method="GET" enctype="multipart/form-data" class="col-md-6 offset-3">
		@csrf
		<input type="text" name="search" placeholder="First Name" class="form-control">
		<button class="btn btn-success ml-auto">Search</button>
		</form>	-->
		<table class="table table-striped table-bordered table-hover" id="myTable">
			<thead>
				<tr>
					<th>Employee ID</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Middle Name</th>
					<th>Address</th>
					<th>Birthdate</th>
					<th>Age</th>
					<th>Contact No.</th>
					<th>Father Name</th>
					<th>Mother Name</th>
					<th>School</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
			<tfoot>
				<tr>
					<td>Employee ID</td>
					<td>First Name</td>
					<td>Last Name</td>
					<td>Middle Name</td>
					<td>Address</td>
					<td>Birtddate</td>
					<td>Age</td>
					<td>Contact No.</td>
					<td>Father Name</td>
					<td>Mother Name</td>
					<td>School</td>
				</tr>
			</tfoot>
		</table>
	</div>



	<div class="row">
		<form action="/employee/search" method="GET" enctype="multipart/form-data" class="col-md-6 offset-3">
		@csrf
		<input type="text" name="search" placeholder="First Name or Last Name" class="form-control">
		<button class="btn btn-success ml-auto">Search</button>
		</form>
		<table class="table table-striped table-bordered table-hover table-responsive">
			<thead>
				<tr>
					<th>Employee ID</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Middle Name</th>
					<th>Address</th>
					<th>Birthdate</th>
					<th>Age</th>
					<th>Contact No.</th>
					<th>Father Name</th>
					<th>Mother Name</th>
					<th>School</th>
					<th>Actions</th>
				</tr>
			</thead>
						@foreach($employees as $employee)
			<tbody>
				<tr>
					<td>{{$employee->id}}</td>
					<td>{{$employee->first_name}}</td>
					<td>{{$employee->last_name}}</td>
					<td>{{$employee->middle_name}}</td>
					<td>{{$employee->address}}</td>
					<td>{{$employee->birthdate}}</td>
					<td>{{$employee->age}}</td>
					<td>{{$employee->contact_no}}</td>
					<td>{{$employee->father_name}}</td>
					<td>{{$employee->mother_name}}</td>
					<td>{{$employee->school}}</td>
					<td>
						<a href="/employee/{{$employee->id}}/edit" class="btn btn-warning mb-2">Edit</a>
						<form action="/employee/{{$employee->id}}" method="POST" enctype="multipart/form-data">
							@csrf
							@method("DELETE")
						<button class="btn btn-danger">Delete</button>
							
						</form>
					</td>
				</tr>
			</tbody>
			@endforeach
			<tfoot>
				<tr>
					<td>Employee ID</td>
					<td>First Name</td>
					<td>Last Name</td>
					<td>Middle Name</td>
					<td>Address</td>
					<td>Birtddate</td>
					<td>Age</td>
					<td>Contact No.</td>
					<td>Father Name</td>
					<td>Mother Name</td>
					<td>School</td>
					<td>Actions</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
@endsection